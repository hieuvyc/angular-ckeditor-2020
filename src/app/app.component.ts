import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  ckeConfig: any;
  mycontent: string;
  @ViewChild('myckeditor') ckeditor: any;

  constructor() {
    this.mycontent = `<p>My html content</p>`;
  }

  ngOnInit() {
    this.ckeConfig = {
      allowedContent: false,
      forcePasteAsPlainText: true
    };
  }

  openImageProcessor($event: any, ckeditor: any) {
    console.log('open dialog component to upload image');

    // do stuff
    // you can open files management, upload, import dialog,...
    // after your_process, you will have and urlFile, pass it to ckeditor like below

    // your_process.then((urlFile) => {
    //   if (urlFile) {
    //     try {
    //       let link = ckeditor.instance.document.createElement("img");
    //       link.setAttribute("alt", "Image");
    //       link.setAttribute("src", urlFile);
    //
    //       ckeditor.instance.insertElement(link);
    //     }
    //     catch (error) {
    //       console.log((<Error>error).message);
    //     }
    //   }
    // });
  }

  onChange($event: any): void {
    console.log("onChange");
  }

  onPaste($event: any): void {
    console.log("onPaste");
  }
}
